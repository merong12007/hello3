@ECHO off

set "URL=http://10.52.254.90:9000"

echo Starting....
%SONARSCANNER% -Dsonar.host.url=%URL% -Dsonar.projectKey=%SONAR_PROJECT_KEY% -Dsonar.analysis.mode=publish -Dsonar.gitlab.commit_sha=%CI_COMMIT_SHA% -Dsonar.projectName=%SONAR_PROJECT_NAME%

EXIT /B ```
